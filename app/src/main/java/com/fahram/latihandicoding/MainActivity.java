package com.fahram.latihandicoding;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity {
    MovieAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView = findViewById(R.id.lvMovie);
        adapter = new MovieAdapter(MainActivity.this);

        final String url = "https://api.themoviedb.org/3/search/movie/";
        final EditText input = findViewById(R.id.inputText);
        Button tombol = findViewById(R.id.button);

        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        tombol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(url)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                GetMovieService service = retrofit.create(GetMovieService.class);
                Call<Respon> call = service.listMovies(input.getText().toString());
                call.enqueue(new Callback<Respon>() {
                    @Override
                    public void onResponse(Call<Respon> call, Response<Respon> response) {
                        Log.d("Respon : ", response.body().toString());
                        Respon respon = response.body();
                        List<ResultsItem> resultsItems = respon.getResults();
                        adapter.setData(resultsItems);
                    }

                    @Override
                    public void onFailure(Call<Respon> call, Throwable t) {
                        Log.d("Respon Error: ", t.toString());
                    }
                });
            }
        });
    }

    interface GetMovieService {
        @GET("?api_key=5a9c18716a8e0b81365e4c780c047e31&language=en-US")
        Call<Respon> listMovies(@Query("query") String nama);
    }
}
