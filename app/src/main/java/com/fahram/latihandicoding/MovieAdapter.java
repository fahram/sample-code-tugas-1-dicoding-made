package com.fahram.latihandicoding;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by fahram on 27/11/18
 */
public class MovieAdapter extends BaseAdapter {


    private List<ResultsItem> movies;
    private Context context;
    private LayoutInflater mInflater;

    public MovieAdapter(Context context) {
        this.context = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(List<ResultsItem> items){
        movies = items;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return   (movies == null) ?  0 : movies.size();
    }

    @Override
    public Object getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        ViewHolder holder = null;

        if (itemView == null) {
            itemView = mInflater.inflate(R.layout.item, null);
            holder = new ViewHolder(itemView);

            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }
        final ResultsItem movie = movies.get(position);
        holder.judul.setText(movie.getTitle());
        holder.deskripsi.setText(movie.getOverview());
        Picasso.get().load("http://image.tmdb.org/t/p/w185/" +movie.getPosterPath()).into(holder.photo);
        try {
            holder.tanggal.setText(movie.getRelease_date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieActivity.class);
                intent.putExtra("MOVIE", movie);
                context.startActivity(intent);
            }
        });
        return itemView;
    }

    static final class ViewHolder {
        TextView judul, deskripsi, tanggal ;
        ImageView photo;
        ViewHolder(View view) {
            judul = view.findViewById(R.id.judul);
            deskripsi = view.findViewById(R.id.deskripsi);
            tanggal = view.findViewById(R.id.tanggal);
            photo = view.findViewById(R.id.poster);
        }
    }
}
