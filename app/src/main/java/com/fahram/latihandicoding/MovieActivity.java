package com.fahram.latihandicoding;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MovieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ResultsItem movie = getIntent().getParcelableExtra("MOVIE");
        ImageView photo = findViewById(R.id.imgMovie);
        TextView title = findViewById(R.id.titleMovie);
        TextView desctiption = findViewById(R.id.descriptionMovie);
        title.setText(movie.getTitle());
        desctiption.setText(movie.getOverview());
        Picasso.get().load("http://image.tmdb.org/t/p/w185/" +movie.getPosterPath()).into(photo);
    }

}
