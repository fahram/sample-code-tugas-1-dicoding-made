package com.fahram.latihandicoding;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ResultsItem implements Parcelable {
	private String overview;
	private String originalLanguage;
	private String original_title;
	private boolean video;
	private String title;
	private List<Integer> genreIds;
	private String poster_path;
	private String backdrop_path;
	private String release_date;
	private double vote_average;
	private double popularity;
	private int id;
	private boolean adult;
	private int voteCount;

	public void setOverview(String overview){
		this.overview = overview;
	}

	public String getOverview(){
		return overview;
	}

	public void setOriginalLanguage(String originalLanguage){
		this.originalLanguage = originalLanguage;
	}

	public String getOriginalLanguage(){
		return originalLanguage;
	}

	public void setOriginal_title(String original_title){
		this.original_title = original_title;
	}

	public String getOriginal_title(){
		return original_title;
	}

	public void setVideo(boolean video){
		this.video = video;
	}

	public boolean isVideo(){
		return video;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setGenreIds(List<Integer> genreIds){
		this.genreIds = genreIds;
	}

	public List<Integer> getGenreIds(){
		return genreIds;
	}

	public void setPosterPath(String posterPath){
		Log.d("Path : ", posterPath);
		this.poster_path = posterPath;
	}

	public String getPosterPath(){
		return poster_path;
	}

	public void setBackdrop_path(String backdrop_path){
		this.backdrop_path = backdrop_path;
	}

	public String getBackdrop_path(){
		return backdrop_path;
	}

	public void setRelease_date(String release_date){
		this.release_date = release_date;
	}

	public String getRelease_date(){
		return release_date;
	}

	public void setVote_average(double vote_average){
		this.vote_average = vote_average;
	}

	public double getVote_average(){
		return vote_average;
	}

	public void setPopularity(double popularity){
		this.popularity = popularity;
	}

	public double getPopularity(){
		return popularity;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setAdult(boolean adult){
		this.adult = adult;
	}

	public boolean isAdult(){
		return adult;
	}

	public void setVoteCount(int voteCount){
		this.voteCount = voteCount;
	}

	public int getVoteCount(){
		return voteCount;
	}

	@Override
 	public String toString(){
		return 
			"ResultsItem{" + 
			"overview = '" + overview + '\'' + 
			",original_language = '" + originalLanguage + '\'' + 
			",original_title = '" + original_title + '\'' +
			",video = '" + video + '\'' + 
			",title = '" + title + '\'' + 
			",genre_ids = '" + genreIds + '\'' + 
			",poster_path = '" + poster_path + '\'' +
			",backdrop_path = '" + backdrop_path + '\'' +
			",release_date = '" + release_date + '\'' +
			",vote_average = '" + vote_average + '\'' +
			",popularity = '" + popularity + '\'' + 
			",id = '" + id + '\'' + 
			",adult = '" + adult + '\'' + 
			",vote_count = '" + voteCount + '\'' + 
			"}";
		}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.overview);
        dest.writeString(this.originalLanguage);
        dest.writeString(this.original_title);
        dest.writeByte(this.video ? (byte) 1 : (byte) 0);
        dest.writeString(this.title);
        dest.writeList(this.genreIds);
        dest.writeString(this.poster_path);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.release_date);
        dest.writeDouble(this.vote_average);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.id);
        dest.writeByte(this.adult ? (byte) 1 : (byte) 0);
        dest.writeInt(this.voteCount);
    }

    public ResultsItem() {
    }

    protected ResultsItem(Parcel in) {
        this.overview = in.readString();
        this.originalLanguage = in.readString();
        this.original_title = in.readString();
        this.video = in.readByte() != 0;
        this.title = in.readString();
        this.genreIds = new ArrayList<Integer>();
        in.readList(this.genreIds, Integer.class.getClassLoader());
        this.poster_path = in.readString();
        this.backdrop_path = in.readString();
        this.release_date = in.readString();
        this.vote_average = in.readDouble();
        this.popularity = in.readDouble();
        this.id = in.readInt();
        this.adult = in.readByte() != 0;
        this.voteCount = in.readInt();
    }

    public static final Parcelable.Creator<ResultsItem> CREATOR = new Parcelable.Creator<ResultsItem>() {
        @Override
        public ResultsItem createFromParcel(Parcel source) {
            return new ResultsItem(source);
        }

        @Override
        public ResultsItem[] newArray(int size) {
            return new ResultsItem[size];
        }
    };
}